import {defineComponent, ref} from 'vue'
import {mainBreakpoint} from "src/helpers/constants";
import {useWeb3Section} from "stores/web3-section";
import {useWalletSection} from "stores/wallet-section";

export default defineComponent({
  name: 'IndexPage',
  setup() {
    const web3Store = useWeb3Section();
    const walletStore = useWalletSection();
    return {
      mainBreakpoint,
      web3Store,
      walletStore,
    }
  }
})
