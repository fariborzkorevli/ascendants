import {defineStore} from 'pinia';
import Web3 from "web3";
import Web3Modal from "web3modal";
import {useWeb3Section} from "stores/web3-section";
import {ASCStakingAbi} from "src/artifacts/contracts/ASCStaking.json";
import {MockAbi} from "src/artifacts/contracts/MockERC1155.json";

let web3Store = null;

export const useWalletSection = defineStore('walletSection', {
  state: () => ({
    connected: false,
    loading: {
      connectToWallet: false,
      fetchAccountData: false,
    },
    walletAddress: null,

    web3Modal: null,
    provider: null,
    web3: null,
    hasMetamask: false
  }),

  getters: {
    isLoading(state) {
      return state.loading.fetchAccountData || state.loading.connectToWallet
    }
  },

  actions: {
    init() {
      // must be called in created hook
      const providerOptions = {};

      this.web3Modal = new Web3Modal({
        cacheProvider: true, // optional
        providerOptions, // required
      });
    },
    async setEventListeners() {
      // Subscribe to accounts change
      this.provider.on("accountsChanged", async (accounts) => {
        console.log("accountsChanged");
        console.log(accounts);
        if (accounts.length > 0) {
          console.log(`Using account ${accounts[0]}`)
          await this.fetchAccountData();
          this.connected = true;
          localStorage.setItem('connected', JSON.stringify(this.connected))
        } else {
          console.error('0 accounts.');
          this.connected = false;
          localStorage.setItem('connected', JSON.stringify(this.connected))
        }
      });

      // Subscribe to chainId change
      this.provider.on("chainChanged", async (chainId) => {
        console.log("chainChanged");
        console.log(chainId);
        await this.fetchAccountData();
      });

      // Subscribe to provider connection
      this.provider.on("connect", (info) => {
        console.log("connected");
        console.log(info);
      });

      // Subscribe to provider disconnection
      this.provider.on("disconnect", async (error) => {
        console.log("disconnected");
        console.log(error);
        await this.web3Modal.clearCachedProvider();
        this.provider = null;
        this.connected = false;
        localStorage.setItem('connected', JSON.stringify(this.connected))
      });
    },
    async checkConnection() {
      // must be called in mounted hook
      // Check if browser is running Metamask
      if (window.ethereum) {
        this.hasMetamask = true;
        // web3 = new Web3(window.ethereum);
        // do not pop up the wallet connection modal if
        // the wallet is disconnected and the user opens the page
        let wasConnected = JSON.parse(localStorage.getItem('connected'));
        if (wasConnected) {
          console.log("wasConnected")
          console.log(wasConnected)
        }

        if (wasConnected && this.web3Modal.cachedProvider) {
          await this.connectToWeb3modal();
        }
      } else if (window.web3) {
        // web3 = new Web3(window.web3.currentProvider);
        // or say to install metamask wallet
        this.hasMetamask = false;
      }
    },
    async fetchAccountData() {
      this.loading.fetchAccountData = true;
      try {
        // Get a Web3 instance for the wallet
        this.web3 = new Web3(this.provider);
        console.log("web3")
        console.log(this.web3);

        const [addresses, networkName, chainId] = await Promise.all([
          this.web3.eth.requestAccounts(),
          this.web3.eth.net.getNetworkType(),
          this.web3.eth.getChainId()
        ]);

        // let addresses = await this.web3.eth.requestAccounts();
        console.log("addresses")
        console.log(addresses)

        this.walletAddress = addresses[0];
        console.log("walletAddress")
        console.log(this.walletAddress)

        let walletBalanceInWei = await this.web3.eth.getBalance(this.walletAddress);
        console.log("walletBalanceInWei")
        console.log(walletBalanceInWei)

        // let networkName = await this.web3.eth.net.getNetworkType();
        console.log("networkName")
        console.log(networkName)

        // let chainId = await this.web3.eth.getChainId();
        console.log("chainId")
        console.log(chainId)
      } catch (error) {
        console.log("catch")
        console.log(error)
      } finally {
        this.loading.fetchAccountData = false;
      }
    },
    /**
     * Fetch account data for UI when
     * - User switches accounts in wallet
     * - User switches networks in wallet
     * - User connects wallet initially
     */
    async refreshAccountData() {
      await this.fetchAccountData(this.provider);
    },
    async connectToWeb3modal() {
      console.log("connect to wallet in store")
      this.loading.connectToWallet = true;
      console.log("connectToWeb3modal", this.web3Modal);
      try {
        this.provider = await this.web3Modal.connect();
      } catch (e) {
        console.log("Could not get a wallet connection", e);
        this.loading.connectToWallet = false;
        return;
      }
      await this.setEventListeners();
      await this.refreshAccountData();
      this.connected = true;
      localStorage.setItem('connected', JSON.stringify(this.connected))

      this.initContract();

      this.loading.connectToWallet = false;
    },
    async disconnectFromWeb3modal() {
      console.log("Killing the wallet connection", this.provider);
      await this.web3Modal.clearCachedProvider();
      this.provider = null;
      this.connected = false;
      localStorage.setItem('connected', JSON.stringify(this.connected))
    },
    initContract() {
      web3Store = useWeb3Section();
      web3Store.initContract();
    }
  }
})
