import {defineStore} from 'pinia';
import {useWalletSection} from "stores/wallet-section";
import {showErrorMessage, showSuccessMessage} from "src/helpers/notify";
// import {ASCStakingAbi} from "src/artifacts/contracts/ASCStaking.json";
// import {MockAbi} from "src/artifacts/contracts/MockERC1155.json";

let walletStore = null;
const {ASCStakingAbi} = require('src/artifacts/contracts/ASCStaking.json');
const {MockAbi} = require('src/artifacts/contracts/MockERC1155.json');
const ASCStakingAddress = '0x220C483F8eAc17f652fB821c742680287d4351b1';
const mockAddress = '0x42539f0554679BC664fF204a24b387bd57ccC6a2';

export const useWeb3Section = defineStore('web3Section', {
  state: () => ({
    ASCStakingContract: null,
    MockContract: null,
    isApprovedForAll: false,
    slots: [
      {
        tokenId: 0,
        slotId: 0,
        name: 'empty',
        img: 'empty-slot.svg',
      },
      {
        tokenId: 1,
        slotId: 1,
        name: 'GIFT_OF_ZEUS',
        img: 'gift_of_ zeus.png',
      },
      {
        tokenId: 2,
        slotId: 2,
        name: 'GIFT_OF_POSEIDON',
        img: 'gift_of_ poseidon.png',
      },
      {
        tokenId: 3,
        slotId: 3,
        name: 'GIFT_OF_HADES',
        img: 'empty-slot.svg',
      },
    ],
    unStakedSlots: [
      {
        tokenId: 0,
        slotId: 0,
        name: 'empty',
        img: 'empty-slot.svg',
        status: 'unStaked',
      },
      {
        tokenId: 0,
        slotId: 1,
        name: 'empty',
        img: 'empty-slot.svg',
        status: 'unStaked',
      },
      {
        tokenId: 0,
        slotId: 2,
        name: 'empty',
        img: 'empty-slot.svg',
        status: 'unStaked',
      },
      {
        tokenId: 0,
        slotId: 3,
        name: 'empty',
        img: 'empty-slot.svg',
        status: 'unStaked',
      },
    ],
    stakedSlots: [
      {
        endDate: "0",
        img: "empty-slot.svg",
        inUse: false,
        name: "empty",
        slotId: 1,
        stakeType: "0",
        startDate: "0",
        status: "empty",
        tokenId: "0",
      },
      {
        endDate: "0",
        img: "empty-slot.svg",
        inUse: false,
        name: "empty",
        slotId: 1,
        stakeType: "0",
        startDate: "0",
        status: "empty",
        tokenId: "0",
      },
      {
        endDate: "0",
        img: "empty-slot.svg",
        inUse: false,
        name: "empty",
        slotId: 1,
        stakeType: "0",
        startDate: "0",
        status: "empty",
        tokenId: "0",
      },
      {
        endDate: "0",
        img: "empty-slot.svg",
        inUse: false,
        name: "empty",
        slotId: 1,
        stakeType: "0",
        startDate: "0",
        status: "empty",
        tokenId: "0",
      },
    ],
    emptySlotIds: [0, 1, 2, 3],
    loading: {
      init: false,
      setApprovalForAll: false,
      getSlots: false,
      stake: false,
      unStake: false,
    },
    currentItem: null,
  }),

  getters: {
    doubleCount(state) {
      return state.counter * 2
    }
  },

  actions: {
    async initContract() {
      this.loading.init = true;
      console.log("web3 stoooore");
      walletStore = useWalletSection();
      [this.ASCStakingContract, this.MockContract] = await Promise.all([
        new walletStore.web3.eth.Contract(ASCStakingAbi, ASCStakingAddress),
        new walletStore.web3.eth.Contract(MockAbi, mockAddress)
      ]);
      this.isApprovedForAll = await this.MockContract.methods.isApprovedForAll(walletStore.walletAddress, ASCStakingAddress).call()
      // if (this.isApprovedForAll) {
      await this.getSlots();
      // }
      this.loading.init = false;
    },
    async setApprovalForAll() {
      this.loading.setApprovalForAll = true;
      const result = await this.MockContract.methods.setApprovalForAll(ASCStakingAddress, true)
        .send({gas: '1000000', from: walletStore.walletAddress})
      if (result.status === true) {
        this.isApprovedForAll = true;
        // await this.getSlots();
      }
      this.loading.setApprovalForAll = false;
    },
    async getSlots() {
      // const balanceOfBatch = await this.MockContract.methods.balanceOfBatch([walletStore.walletAddress, walletStore.walletAddress, walletStore.walletAddress], [1, 2, 3]).call()
      // this.stakedSlots = await this.ASCStakingContract.methods.getSlots(walletStore.walletAddress).call()
      this.loading.getSlots = true;
      let balanceOfBatch;
      let stakedSlotsResult;
      [balanceOfBatch, stakedSlotsResult] = await Promise.all([
        this.MockContract.methods.balanceOfBatch([walletStore.walletAddress, walletStore.walletAddress, walletStore.walletAddress], [1, 2, 3]).call(),
        this.ASCStakingContract.methods.getSlots(walletStore.walletAddress).call()
      ]);

      let unStakedSlotsCount = 0;
      this.unStakedSlots = [];
      balanceOfBatch.forEach((value, index) => {
        for (let i = 0; i < value; ++i) {
          this.unStakedSlots.push({
            tokenId: index + 1,
            slotId: unStakedSlotsCount,
            name: this.slots[index + 1].name,
            img: this.slots[index + 1].img,
            status: 'unStaked'
          })
          ++unStakedSlotsCount;
        }
      })
      for (let i = unStakedSlotsCount; i < 4; ++i) {
        this.unStakedSlots.push({
          tokenId: 0,
          slotId: unStakedSlotsCount,
          name: this.slots[0].name,
          img: this.slots[0].img,
          status: 'empty'
        })
        ++unStakedSlotsCount;
      }

      this.stakedSlots = [];
      this.emptySlotIds = [];
      stakedSlotsResult.forEach((value, index) => {
        if (value.inUse === false) {
          this.emptySlotIds.push(index)
        }
        this.stakedSlots.push({
          tokenId: value.tokenId,
          slotId: index,
          name: this.slots[value.tokenId].name,
          img: this.slots[value.tokenId].img,
          endDate: value.endDate,
          endDateHuman: new Date(value.endDate),
          inUse: value.inUse,
          stakeType: value.stakeType,
          startDate: value.startDate,
          startDateHuman: new Date(value.startDate),
          status: value.inUse ? 'staked' : 'empty',
        })
      })

      console.log("this.unStakedSlots")
      console.log(this.unStakedSlots)
      console.log("this.stakedSlots")
      console.log(this.stakedSlots)
      console.log(stakedSlotsResult)
      this.loading.getSlots = false;
    },
    async stake(tokenIds, stakingTypes) {
      this.loading.stake = true;
      console.log("tokenId")
      console.log(tokenIds)
      console.log("stakingType")
      console.log(stakingTypes)
      try {
        const result = await this.ASCStakingContract.methods.stake(tokenIds, stakingTypes, [this.emptySlotIds[0]])
          .send({gas: '1000000', from: walletStore.walletAddress})
        console.log("result")
        console.log(result)
        showSuccessMessage("Successfully staked")
        this.currentItem = null;
        await this.getSlots();
      } catch (error) {
        console.log("Error in stake")
        console.log(error)
        const errorMsg = error?.message ? error.message : "Error in stake";
        showErrorMessage(errorMsg)
      } finally {
        this.loading.stake = false;
      }
    },
    async unStake(slotIds) {
      this.loading.unStake = true;
      console.log("slotId")
      console.log(slotIds)
      try {
        const result = await this.ASCStakingContract.methods.unStake(slotIds)
          .send({gas: '1000000', from: walletStore.walletAddress})
        console.log("result")
        console.log(result)
        showSuccessMessage("Successfully un staked")
        this.currentItem = null;
        await this.getSlots();
      } catch (error) {
        console.log("Error in unStake")
        console.log(error)
        const errorMsg = error?.message ? error.message : "Error in un stake";
        showErrorMessage(errorMsg)
      } finally {
        this.loading.unStake = false;
      }
    }
  }
})
