import {defineStore} from 'pinia';

export const useScrollSection = defineStore('scrollSection', {
  state: () => ({
    section: '',
  }),
  getters: {
    // doubleCount: (state) => state.counter * 2,
  },
  actions: {
    setScrollSection(sectionName) {
      this.section = sectionName;
    },
  },
});
